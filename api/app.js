const Joi = require('joi'); 
const express = require('express');
const app = express();
var cors = require('cors');

app.use(express.json()) // for parsing application/json
app.use(cors());

// Base route
app.post('/', (req, res,next) => {
    res.send("test");
});

// Add restful routes
app.use('/api',require('./routes/auth'));
app.use('/api',require('./routes/guest'));


// Configure the server
const port = process.env.PORT || 8000;
app.listen(port, () => {
    console.log(`Serving to port ${port}`);
});