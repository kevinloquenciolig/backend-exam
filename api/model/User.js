const connection = require("../connection");

class User {

	constructor() {
		this.name = "";
		this.password = "";
		this.email = "";
		this.created_at = "";
		this.updated_at = "";
    }

    save()
    {
        return saveUser(this);
    }

    info()
    {
        return info(this.id);
    }
    
}


// Save user information
async function saveUser(user) {

    if(!(user instanceof User)) throw new Error("Parameter must be instance of User model");

    var userId = await new Promise((resolve, reject) => {

		let create_query = `insert into users(name,email,password)`;
		create_query += ` values (?,?,?)`;

		connection.query(create_query, [

			user.name,
			user.email,
			user.password,

		], (error, results) => {

            if (error) return reject(error);

			resolve(results.insertId);

		});
    });
    
    var user = await info(userId);
 
    console.log("testing1");
	return user;
}

// Get the user information
function info(userId) {

    const user = new User;

	return new Promise((resolve, reject) => {

		connection.query(`select * from users where id=?`, [userId], (error, results) => {

			if (error) return reject(error);

			var result = results[0];
			user.id = result.id;
			user.name = result.name;
			user.email = result.email;
			user.password = result.password;
			user.created_at = result.created_at;
			user.updated_at = result.updated_at;
			resolve(user);
		});
	});
}


module.exports = User;
