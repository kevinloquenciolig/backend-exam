const express = require('express');
const router = express.Router();
const Joi = require('joi');

const User = require('../model/User');


router.post('/register', (request, response) => {
    
    // Validation rules
    const schema = Joi.object().keys({

        name: Joi.string().max(255).required(),
        email: Joi.string().min(4).required(),
        password: Joi.string().min(6).required(),
        password_confirmation: Joi.string().min(6).required()

    });

    const {
        body
    } = request;

    const {
        error
    } = schema.validate(body, {
        abortEarly: false
    });


    // Validate user information
    if (error) {

        let validationError={
            message: "The given data was invalid.",
            errors: {}
        };

        error.details.forEach(fields => {
            validationError.errors[fields.context.label] = [fields.message];
        });;

        return response.status(422).json(validationError);
    }

    // Populate user information from request
    var user = new User;
    user.name = body.name;
    user.email = body.email;
    user.password = body.password;

    // Save user information to database
    user.save().then(user=>{ 

        return response.status(201).json({
            name: user.name,
            email: user.email,
            updated_at: user.updated_at,
            created_at: user.created_at,
            id: user.id,
        });

    }).catch(error=>{
        response.status(500).send(error.message);
    });
    
});

router.post('/login', (request, response) => {

});


router.get('/posts', (request, response) => {

});

router.get('/posts/{post}', (request, response) => {

});

router.get('/posts/{post}/comments', (request, response) => {

});



module.exports = router;